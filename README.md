# All my Merge Requests by far

## Contribution to Technical Documentation

| MR no | Repo | Context |
| ------ | ------ | ------ |
| <a href="http://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/108174/" target="_blank">!108174</a> | Gitlab.com |First MR, added content to the Gitlab.com website | 
| <a href="http://gitlab.com/gitlab-org/gitlab/-/merge_requests/93813/" target="_blank">!93813</a> | Gitlab.org | Fixed some broken links of doc(**Gitlab Hackathon 2022 Q3**) |
| <a href="http://gitlab.com/gitlab-org/gitlab/-/merge_requests/94299" target="_blank">!94299</a> | Gitlab.org | Wrote major content for a newly made webapage "Namespace" in doc(**Gitlab Hackathon 2022 Q3**) |
| <a href="http://gitlab.com/gitlab-org/gitlab/-/merge_requests/96031" target="_blank">!96031</a> | Gitlab.org | Redirected many broken links to an index page in doc |
| <a href="https://github.com/meshery/meshery/pull/6171" target="_blank">#6171</a> | Meshery | Fixed broken link contribution guide doc |
| <a href="https://github.com/meshery/meshery/pull/6170" target="_blank">#6170</a> | Meshery | Made the doc more concise by removing irrelevant content |
| <a href="https://github.com/meshery/meshery/pull/6184" target="_blank">#6184</a> | Meshery | Fixed the depreciated commands and pre-requisites to setup local dev env|
| <a href="https://github.com/layer5io/layer5/pull/3158" target="_blank">#3158</a> | Layer5 | Add the community handbook link for newcomers |

<!-- remove the "s" from "https" to enable the taget=blank attribute-->



